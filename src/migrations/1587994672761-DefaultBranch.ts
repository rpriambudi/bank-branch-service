import {MigrationInterface, QueryRunner} from "typeorm";

export class DefaultBranch1587994672761 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("insert into branch values (1, '001', 'Default Branch', 'Default Branch')");
        await queryRunner.query("insert into branch values(99999, '999', 'IB_CLIENT', 'IB_CLIENT')");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('delete from branch where id = 1');
        await queryRunner.query('delete from branch where id = 99999');
    }

}

import { Branch } from './../../entities/branch.entity';
import { CreateBranchDto } from './../../dto/create-branch.dto';

export interface BranchService {
    createNewBranch(branchDto: CreateBranchDto): Promise<Branch>;
    findBranchByCode(branchCode: string): Promise<Branch>;
    findBranchById(branchId: number): Promise<Branch>;
}
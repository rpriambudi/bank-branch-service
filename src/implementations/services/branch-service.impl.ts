import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Branch } from './../../entities/branch.entity';
import { CreateBranchDto } from './../../dto/create-branch.dto';
import { BranchService } from './../../interfaces/services/branch-service.interface';
import { BranchNotFoundException } from './../../exceptions/branch-not-found.exception';

@Injectable()
export class BranchServiceImpl implements BranchService {
    constructor(
        @InjectRepository(Branch) private readonly repository: Repository<Branch>
    ) {}

    async createNewBranch(branchDto: CreateBranchDto): Promise<Branch> {
        const branch = this.repository.create(branchDto);
        return await this.repository.save(branch);
    }    
    
    async findBranchByCode(branchCode: string): Promise<Branch> {
        const branch = await this.repository.findOne({ branchCode: branchCode });
        if (!branch)
            throw new BranchNotFoundException('Branch not found with requested code.');

        return branch;
    }
    
    async findBranchById(branchId: number): Promise<Branch> {
        const branch = await this.repository.findOne({ id: branchId });
        if (!branch)
            throw new BranchNotFoundException('Branch not found with requested code.');

        return branch;
    }
    
}
import { Controller, Get, Inject, Post, Body, Param } from '@nestjs/common';
import { BranchService } from './interfaces/services/branch-service.interface';
import { CreateBranchDto } from './dto/create-branch.dto';

@Controller()
export class AppController {
  constructor(
    @Inject('BranchService') private readonly branchService: BranchService
  ) {}

  @Post('/api/branch')
    async registerNewBranch(@Body() branchDto : CreateBranchDto) {
        const branchData = await this.branchService.createNewBranch(branchDto);
        return branchData;
    }

    @Get('/api/branch/:id')
    async getBranchById(@Param('id') id : number) {
        const branchData = await this.branchService.findBranchById(id);
        return branchData;
    }

    @Get('/api/branch/:branchCode/code')
    async getBranchByBranchCode(@Param('branchCode') branchCode : string) {
        const branchData = await this.branchService.findBranchByCode(branchCode);
        return branchData;
    }
}

import { GenericException } from 'bank-shared-lib';

export class BranchNotFoundException extends GenericException {
    getDisplayCode(): string {
        return 'BRANCH_NOT_FOUND';
    }

    getErrorCode(): string {
        return '400101';
    }

    constructor(message: string) {
        super(message);
    }
}
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Branch {
    @PrimaryGeneratedColumn()
    id: number

    @Column({
        type: 'varchar',
        unique: true,
        name: 'branch_code'
    })
    branchCode: string

    @Column({
        type: 'varchar',
        unique: true,
        name: 'name'
    })
    name: string

    @Column({
        type: 'text',
        name: 'address'
    })
    address: string
}
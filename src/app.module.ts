import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { Branch } from './entities/branch.entity';
import { DatabaseConfig, AuthConfig } from './configs/configuration';
import { BranchServiceImpl } from './implementations/services/branch-service.impl';
import { CsAuthMiddleware } from './middlewares/cs-auth.middleware';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get<string>('database.host'),
          port: configService.get<number>('database.port'),
          username: configService.get<string>('database.username'),
          password: configService.get<string>('database.password'),
          database: configService.get<string>('database.name'),
          synchronize: true,
          autoLoadEntities: true
        }
      },
      inject: [ConfigService]
    }),
    TypeOrmModule.forFeature([Branch]),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [DatabaseConfig, AuthConfig]
    })
  ],
  controllers: [AppController],
  providers: [
    {
      provide: 'BranchService',
      useClass: BranchServiceImpl
    }
  ],
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(CsAuthMiddleware)
      .exclude(
        {
          path: '/api/branch/:id',
          method: RequestMethod.GET
        },
        {
          path: '/api/branch/:branchCode/code',
          method: RequestMethod.GET
        }
      )
      .forRoutes(AppController)
  }
}

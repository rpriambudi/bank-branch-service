import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpGenericFilter } from 'bank-shared-lib';
import exceptionCode from './../exception-code.json';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new HttpGenericFilter(exceptionCode));
  await app.listen(3000);
}
bootstrap();

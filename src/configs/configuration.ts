export const DatabaseConfig = () => ({
    database: {
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        name: process.env.DB_NAME
    }
})

export const AuthConfig = () => ({
    auth: {
        jwksUrl: process.env.KEYCLOAK_JWKS_URL,
        clientId: process.env.KEYCLOAK_CLIENT_ID,
    }
})
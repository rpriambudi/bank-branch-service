# Base OS
FROM node:10-alpine

# Working directory
WORKDIR /usr/src/bank-branch-service

COPY package*.json ./

RUN apk add --no-cache bash

RUN npm install

COPY . .

EXPOSE 3000
